<?php
require_once __DIR__."/../vue/VueResultat.php";

/**
* Cette classe permet de contrôler le résultat de la partie
* @author MAHAUDA Théo, DEGREZ Clara
* @category Resultat
* @package Controleur
* @version v.1.0 (03/12/2017)
*/
class ControleurResultat
{
  /**
  * @var <VueResultat> $vuePartie qui permet de gérer la vue résultat
  */
  private $vueResultat;
  /**
  * @var <ModeleJoueur> $modeleJoueur qui permet de gérer le joueur
  */
  private $modeleJoueur;

  /**
  * Constructeur qui récupére le joueur de la session crée dans accueil et instancie une vue résultat
  */
  public function __construct()
  {
    $this->modeleJoueur = $_SESSION["joueur"];
    $this->vueResultat = new VueResultat();
  }

  /**
  * Méthode qui demande l'affichage du résultat de la partie
  * @param <string> $message représente une victoire ou une défaite
  */
  public function demanderAfficherResultats($message)
  {
    $this->vueResultat->afficherResultats($message,$this->modeleJoueur->getResultats(),$this->modeleJoueur->getClassement());
  }

}
?>
