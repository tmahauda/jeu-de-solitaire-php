<?php
require_once __DIR__."/../vue/VueErreur.php";

/**
* Cette classe permet de contrôler les erreurs relatives à la partie ou à l'accueil
* @author MAHAUDA Théo, DEGREZ Clara
* @category Erreur
* @package Controleur
* @version v.1.0 (03/12/2017)
*/
class ControleurErreur
{

  /**
  * @var <VueErreur> $vueErreur qui permet de gérer la vue des erreurs
  */
  private $vueErreur;

  /**
  * Constructeur qui instancie une vue erreur
  */
  public function __construct()
  {
    $this->vueErreur = new VueErreur();
  }

  /**
  * Méthode qui demande l'affichage de l'erreur issu de la partie
  * @param <string> $message représente le message d'erreur
  */
  public function demanderAfficherErreurPartie($message)
  {
    $this->vueErreur->afficherErreurPartie($message);
  }

  /**
  * Méthode qui demande l'affichage de l'erreur issu de l'accueil
  * @param <string> $message représente le message d'erreur
  */
  public function demanderAfficherErreurAccueil($message)
  {
    $this->vueErreur->afficherErreurAccueil($message);
  }

}
?>
