<?php
require_once __DIR__."/../vue/VueAuthentification.php";
require_once __DIR__."/../modele/ModeleJoueur.php";

/**
* Cette classe permet de lancer une exception relative à la page d'accueil
* @author MAHAUDA Théo, DEGREZ Clara
* @category Accueil
* @package Exception
* @version v.1.0 (03/12/2017)
*/
class ExceptionAccueil extends Exception
{
  /**
  * Constructeur qui crée l'exception relative à l'accueil
  * @param <string> $message représente le message d'erreur
  */
  public function __construct($message)
  {
    $this->message = $message;
  }
}

/**
* Cette classe permet de controler les interactions avec l'accueil
* @author MAHAUDA Théo, DEGREZ Clara
* @category Accueil
* @package Controleur
* @version v.1.0 (03/12/2017)
*/
class ControleurAccueil
{
  /**
  * @var <VueAuthentification> $vueAccueil permet d'afficher la vue d'authentification pour jouer à une partie
  */
  private $vueAccueil;

  /**
  * Constructeur qui démarre une session et instancie une nouvelle vue d'authentification dans accueil
  */
  public function __construct()
  {
    session_start();
    $this->vueAccueil = new VueAuthentification();
  }

  /**
  * Méthode qui demande l'affichage de l'authentification à la vue
  * @param <string> $message représente le message personnalisé (Bienvenu ou Merci)
  */
  public function demanderAfficherAccueil($message)
  {
    $this->vueAccueil->afficherAuthentification($message);
  }

  /**
  * Méthode qui demande la déconnexion de la session
  */
  public function demanderDeconnecterSession()
  {
    session_unset(); // On détruit les variables de la session
    session_destroy(); // On détruit la session
  }

  /**
  * Méthode qui demande une connexion si le joueur existe dans la BD en l'instanciant puis qui l'enregistre dans une session
  * @param <string> $pseudo représente le pseudo du joueur
  * @param <string> $mdp représente le mot de passe du joueur
  * @throws ExceptionAccueil s'il y a eu un problème d'authentification du joueur
  */
  public function demanderConnexion($pseudo,$mdp)
  {
    try
    {
      if(!empty($pseudo) && !empty($mdp))
      {
        $modeleJoueur = new ModeleJoueur($pseudo,$mdp);
        $_SESSION["joueur"] = $modeleJoueur;
      }
      else throw new ExceptionAccueil("Les champs doivent être complétés");
    }
    catch(ExceptionJoueur $e)
    {
      throw new ExceptionAccueil($e->getMessage());
    }
  }

  /**
  * Méthode qui demande l'inscription d'un nouveau joueur dans la BD
  * @param <string> $pseudo représente le pseudo du joueur
  * @param <string> $mdp représente le mot de passe du joueur
  * @param <string> $mdpConfirmation représente le mot de passe à confirmer du joueur
  * @throws ExceptionAccueil s'il y a eu un problème d'inscription du joueur
  */
  public function demanderInscription($pseudo,$mdp,$mdpConfirmation)
  {
    if(!empty($pseudo) && !empty($mdp) && !empty($mdpConfirmation))
    {
      if($mdp == $mdpConfirmation)
      {
        try
        {
          $this->modeleJoueur = new ModeleNewJoueur($pseudo,$mdp);
        }
        catch(ExceptionJoueur $e)
        {
          throw new ExceptionAccueil($e->getMessage());
        }
      }
      else throw new ExceptionAccueil("Les mots de passe sont différents. Re-saisir");
    }
    else throw new ExceptionAccueil("Les champs doivent être complétés");
  }

}
?>
