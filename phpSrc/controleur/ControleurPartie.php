<?php
require_once __DIR__."/../vue/VuePartie.php";
require_once __DIR__."/../modele/ModeleJoueur.php";
require_once __DIR__."/../modele/ModelePlateau.php";

/**
* Cette classe permet de lancer une exception relative à la partie en cours
* @author MAHAUDA Théo, DEGREZ Clara
* @category Partie
* @package Exception
* @version v.1.0 (03/12/2017)
*/
class ExceptionPartie extends Exception
{
  /**
  * Constructeur qui crée l'exception relative à la partie en cours
  * @param <string> $message représente le message d'erreur
  */
  public function __construct($message)
  {
    $this->message = $message;
  }
}

/**
* Cette classe permet de lancer une exception relative au résultat de la partie en cours
* @author MAHAUDA Théo, DEGREZ Clara
* @category Partie
* @package Exception
* @version v.1.0 (03/12/2017)
*/
class ExceptionResultat extends Exception
{
  /**
  * Constructeur qui crée l'exception relative au résultat de la partie en cours
  * @param <string> $message représente le message d'erreur
  */
  public function __construct($message)
  {
    $this->message = $message;
  }
}

/**
* Cette classe permet de contrôler les interactions avec la partie en cours
* @author MAHAUDA Théo, DEGREZ Clara
* @category Partie
* @package Controleur
* @version v.1.0 (03/12/2017)
*/
class ControleurPartie
{
  /**
  * @var <VuePartie> $vuePartie qui permet de gérer la vue de la partie
  */
  private $vuePartie;

  /**
  * @var <ModelePlateau> $modelePlateau qui permet de gérer le plateau
  */
  private $modelePlateau;

  /**
  * @var <ModeleJoueur> $modeleJoueur qui permet de gérer le joueur
  */
  private $modeleJoueur;

  /**
  * Constructeur qui démarre une session et instancie un nouveau plateau s'il n'existe pas dans la variable session et vérifie si le joueur s'est bien authentifié avant de jouer
  */
  public function __construct()
  {
    // On démarre la session
    session_start();

    // Si le joueur a bien été authentifié
    if(isset($_SESSION["joueur"]))
    {
      // On récupère le modèle du joueur dans la session déjà configuré dans la méthode verifierConnexion($pseudo,$mdp) de la classe ControleurAccueil
      $this->modeleJoueur = $_SESSION["joueur"];

      // Si le plateau existe déjà, alors on le récupére
      if(isset($_SESSION["plateau"]))
      {
        $this->modelePlateau = $_SESSION["plateau"];
      }
      // Sinon on crée un nouveau plateau
      else $this->modelePlateau = new ModelePlateau();

      // On instancie une vue partie
      $this->vuePartie = new VuePartie();
    }
    else throw new ExceptionPartie("Vous n'êtes pas authentifié. Veuillez vous identifier à l'acceuil svp");
  }

  /**
  * Méthode qui demande l'affichage de la partie avec un message personalisé, le pseudo du joueur et le plateau
  * @param <string> $message représente le message personnalisé
  */
  public function demanderAfficherPartie($message)
  {
    $this->vuePartie->afficherPartie($message, $this->modeleJoueur->getPseudo(), $this->modelePlateau->getPlateau());
  }

  /**
  * Méthode qui permet d'annuler son coup d'avant pour revenir à l'état du plateau avant déplacement
  */
  public function demanderAnnulerCoup()
  {
    // On modifie le tableau du plateau avant déplacement grâce à la variable temporaire du plateau dans session
    $this->modelePlateau->setPlateau($_SESSION["plateauTMP"]);
    // On sauvegarde le plateau avant déplacement
    $_SESSION["plateau"] = $this->modelePlateau;
    // On détruit la variable temporaire du plateau dans session car on ne peut effectuer qu'un seul recule en arrière
    unset($_SESSION["plateauTMP"]);
  }

  /**
  * Méthode qui demande d'enlever la première bille du plateau lorsqu'on débute la partie par ses coordonnées x et y,
  * puis de sauvegarder l'état du plateau (si la bille a bien été supprimée)
  * @param <integer> $ligne représente la coordonnée sur x du plateau
  * @param <integer> $colonne représente la coordonnée sur y du plateau
  * @throws ExceptionBille s'il y a eu un problème avec la bille que l'on souhaite supprimer
  */
  public function demanderEnleverBille($ligne,$colonne)
  {
    try
    {
      $this->modelePlateau->enleverBille($ligne,$colonne);
      $_SESSION["plateau"] = $this->modelePlateau;
    }
    catch(ExceptionBille $e)
    {
      throw new ExceptionPartie($e->getMessage());
    }
  }

  /**
  * Méthode qui demande de déplacer une bille d'une case d'origine à une case d'arrivé par les coordonnées x et y de chaque case
  * Puis de vérifier s'il reste une unique bille et s'il existe au moins un coup possible sur le plateau après cette opération
  * Enfin de sauvegarder l'état du plateau (s'il ne reste pas une unique bille sur le plateau et qu'il existe des coups possible de déplacement)
  * @param <integer> $ligneDep représente la coordonnée sur x de la case d'origine du plateau
  * @param <integer> $colonneDep représente la coordonnée sur y de la case d'origine du plateau
  * @param <integer> $ligneArr représente la coordonnée sur x de la case objective du plateau
  * @param <integer> $colonneArr représente la coordonnée sur y de la case objective du plateau
  * @throws ExceptionBille s'il y a eu un problème avec la bille que l'on souhaite déplacer
  * @throws ExceptionTermine si la partie est terminée (lorsqu'il ne reste qu'une seule bille ou qu'il n'y a plus de coup possible)
  */
  public function demanderDeplacerBille($ligneDep,$colonneDep,$ligneArr,$colonneArr)
  {
    // Si tout se passe bien
    try
    {
      // On stocke temporairement le plateau au cas où que le joueur demande d'annuler son coup
      $_SESSION["plateauTMP"] = $this->modelePlateau->getPlateau();
      // On déplace la bille vers sa nouvelle case
      $this->modelePlateau->deplacerBille($ligneDep,$colonneDep,$ligneArr,$colonneArr);
      // On vérifie s'il reste une unique bille
      $this->modelePlateau->existeUneUniqueBille();
      // On vérifie s'il reste au moins un coup possible à réaliser
      $this->modelePlateau->existeCoupPossible();
      // On stocke le plateau après avoir déplacé la bille dans la session
      $_SESSION["plateau"] = $this->modelePlateau;
    }
    // Sinon si il y a eu un problème avec le déplacement de la bille
    catch(ExceptionBille $e)
    {
      // On lève une exception de type Partie avec une indication du problème
      throw new ExceptionPartie($e->getMessage());
    }
    // Sinon si la partie est terminée
    catch(ExceptionTermine $e)
    {
      // On enregistre les performances du joueur (gagnée ou non)
      $this->modeleJoueur->enregistrerPerformance($this->modelePlateau->getGagnee());
      // On lève une exception de type Resultat avec le message de victoire ou défaite
      throw new ExceptionResultat($e->getMessage());
    }
  }
}
?>
