<?php
require_once "ControleurAccueil.php";
require_once "ControleurPartie.php";
require_once "ControleurResultat.php";
require_once "ControleurErreur.php";

/**
* Cette classe permet de router la requête du client
* @author MAHAUDA Théo, DEGREZ Clara
* @category Routeur
* @package Controleur
* @version v.1.0 (03/12/2017)
*/
class Routeur
{
  /**
  * @var <ControleurX> $requete qui permet d'instancier un contrôleur X (Accueil, Partie, Resultat ou Erreur) en fonction de la requête émise par le client
  */
  private $requete;

  /**
  * Méthode qui permet d'orienter la requête vers un controleur de type :
  * - Partie si on essaye de déplacer des billes du plateau ou Erreur si la requête comporte une exception sur ce déplacement ou Resultat si la partie est terminée
  * - Partie si on est essaye d'enlever la première bille du plateau ou Erreur si on n'a pas pu capturer une bille
  * - Partie si on essaye de réinitialiser le plateau ou que l'on souhaite rejouer ou Erreur si on essaye de jouer sans se connecter
  * - Partie si on revient d'une page d'erreur issue de la partie
  * - Partie si on essaye d'annuler son coup d'avant
  * - Accueil si on essaye de se connecter à une partie
  * - Accueil si on essaye de s'inscrire
  * - Accueil si tout les cas cités ci-dessus n'ont pas été réalisés
  */
  public function routerRequete()
  {
    // Si on essaye de déplacer des billes du plateau
    if(isset($_POST["ligneDep"]) && isset($_POST["colonneDep"])
    && isset($_POST["ligneArr"]) && isset($_POST["colonneArr"]))
    {
      // Alors on vérifie si son déplacement est réalisable par le contrôleur Partie et on affiche le plateau avec un message de confirmation
      try
      {
        $this->requete = new ControleurPartie();
        $this->requete->demanderDeplacerBille($_POST["ligneDep"],$_POST["colonneDep"],$_POST["ligneArr"],$_POST["colonneArr"]);
        $this->requete->demanderAfficherPartie("Déplacement confirmé");
      }
      // Sinon on lève une exception Partie gérée par le controleur Erreur lorsqu'il y a une anomalie
      catch(ExceptionPartie $e)
      {
        $this->requete = new ControleurErreur();
        $this->requete->demanderAfficherErreurPartie($e->getMessage());
      }
      // Sinon on lève une exception Résultat gérée par le controleur Resultat lorsqu'on a perdu ou gagner la partie
      catch(ExceptionResultat $e)
      {
        $this->requete = new ControleurResultat();
        $this->requete->demanderAfficherResultats($e->getMessage());
      }
    }

    // Sinon si on est essaye d'enlever la première bille du plateau
    else if(isset($_POST["ligneSup"]) && isset($_POST["colonneSup"]))
    {
      // Alors on vérifie si la capture est réalisable et on affiche le plateau modifié avec un message de confirmation
      try
      {
        $this->requete = new ControleurPartie();
        $this->requete->demanderEnleverBille($_POST["ligneSup"],$_POST["colonneSup"]);
        $this->requete->demanderAfficherPartie("Bille supprimée");
      }
      // Sinon on lève une exception Partie gérée par le controleur Erreur lorsqu'on n'a pas pu capturer une bille
      catch(ExceptionPartie $e)
      {
        $this->requete = new ControleurErreur();
        $this->requete->demanderAfficherErreurPartie($e->getMessage());
      }
    }

    // Sinon si on a réinitialiser la partie ou qu'on veut rejouer une nouvelle partie
    else if(isset($_POST["Reinitialiser"]) || isset($_GET["Rejouer"]))
    {
      // Alors on détruit le plateau existant de la session
      session_start();
      unset($_SESSION["plateau"]);
      // Puis on affiche un nouveau plateau avec un message de confirmation
      try
      {
        $this->requete = new ControleurPartie();
        $this->requete->demanderAfficherPartie("Le plateau a été remis à zéro. Bonne chance");
      }
      // Sinon on lève une exception Partie gérée par le controleur Erreur si on essaye de jouer à la partie sans être authentifié à cause de la méthode GET (un utilisateur averti peut écrire dans l'URL index.php?Rejouer sans être connecté)
      catch(ExceptionPartie $e)
      {
        $this->requete = new ControleurErreur();
        $this->requete->demanderAfficherErreurAccueil($e->getMessage());
      }
    }

    // Sinon si on revient d'une page d'erreur
    else if(isset($_POST["Partie"]))
    {
      // On réaffiche le plateau sans modification apportée avec un message de confirmation
      $this->requete = new ControleurPartie();
      $this->requete->demanderAfficherPartie("Re-saisir les coordonnées");
    }

    // Sinon si on essaye d'annuler son coup d'avant, alors on revient dans l'état du plateau avant déplacement
    // et on l'affiche avec un message de confirmation
    else if(isset($_POST["Annuler"]))
    {
      $this->requete = new ControleurPartie();
      $this->requete->demanderAnnulerCoup();
      $this->requete->demanderAfficherPartie("Le déplacement précédent a été annulé");
    }

    // Sinon si on essaye de se connecter à une partie
    else if(isset($_POST["Cpseudo"]) && isset($_POST["Cmdp"]))
    {
      // Alors on vérifie si la connexion peut s'établir gérer par le controleur Accueil
      // et on démarre une nouvelle partie gérer par le controleur Partie en affichant les règles du jeu
      try
      {
        $this->requete = new ControleurAccueil();
        $this->requete->demanderConnexion($_POST["Cpseudo"],$_POST["Cmdp"]);
        $this->requete = new ControleurPartie();
        $this->requete->demanderAfficherPartie("L'objectif du jeu est de supprimer des billes du plateau afin qu'à la fin il n'en reste plus qu'une. Pour qu'une bille soit ôtée du plateau, on doit sauter par dessus avec une autre bille en direction d'une case vide. Si vous ne pouvez plus ôter de bille alors qu'il vous en reste plus d'une, la partie est terminée et vous avez perdu. La prise en diagonale est interdite. Bonne chance !");
      }
      // Sinon on lève une exception Accueil gérée par le controleur Erreur lorsqu'on a un problème pour se connecter à une partie
      catch(ExceptionAccueil $e)
      {
        $this->requete = new ControleurErreur();
        $this->requete->demanderAfficherErreurAccueil($e->getMessage());
      }
    }

    // Sinon si on essaye de s'inscrire
    else if(isset($_POST["Ipseudo"]) && isset($_POST["Imdp"]))
    {
      // Alors on vérifie si l'inscription est réalisable géré par le controleur Accueil
      try
      {
        $this->requete = new ControleurAccueil();
        $this->requete->demanderInscription($_POST["Ipseudo"],$_POST["Imdp"],$_POST["Icmdp"]);
        $this->requete->demanderAfficherAccueil("Vous êtes bien enregistré(e). Veuillez à présent vous connecter");
      }
      // Sinon on lève une exception Accueil gérée par le controleur Erreur lorsqu'on a un problème d'inscription
      catch(ExceptionAccueil $e)
      {
        $this->requete = new ControleurErreur();
        $this->requete->demanderAfficherErreurAccueil($e->getMessage());
      }
    }

    //Sinon dans tout les cas, on affiche par défaut la page d'authentification géré par le controleur Accueil
    else
    {
      $this->requete = new ControleurAccueil();
      $this->requete->demanderDeconnecterSession();
      if(isset($_POST["Quitter"]) || isset($_GET["Quitter"]))
      {
        $this->requete->demanderAfficherAccueil("Merci d'avoir joué. A très vite");
      }
      else $this->requete->demanderAfficherAccueil("Bonjour, bienvenue dans le jeu du Solitaire");
    }
  }
}
?>
