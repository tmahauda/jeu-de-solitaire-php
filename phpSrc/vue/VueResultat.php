<?php

/**
* Cette classe permet d'afficher les résultats de la partie
* @author MAHAUDA Théo, DEGREZ Clara
* @category Resultat
* @package Vue
* @version v.1.0 (03/12/2017)
*/
class VueResultat
{

  /**
  * Méthode qui affiche les résultats de la partie à travers une page HTML
  * @param <string> $message représente un texte personnalisé entre chaque requête émise
  * @param <string> $resultats représente les résultats du joueur courant
  * @param <string> $classement représente le classement des 3 meilleurs joueurs avec leurs résultats
  */
  public function afficherResultats($message,$resultats,$classement)
  {
    header('Content-Type: text/html; charset=utf-8');
    ?>
    <!doctype html>

    <html>

      <head>

        <meta charset="utf-8">
        <title> Resultats </title>

      </head>

      <body>

        <h1 align="center"> <?php echo $message; ?> </h1>

        <h3> Voici vos résultats <?php echo $resultats["pseudo"]; ?> : </h3>
        <p> Nombre de partie joué : <?php echo $resultats["nombrePartie"]; ?> </p>
        <p> Nombre de partie gagnée : <?php echo $resultats["partieGagnee"]; ?> </p>
        <p> Nombre de partie perdu : <?php echo $resultats["partiePerdu"]; ?> </p>
        <p> Pourcentage de partie gagnée : <?php echo $resultats["pourcentageGagnee"]; ?> % </p>
        <p> Pourcentage de partie perdu : <?php echo $resultats["pourcentagePerdu"]; ?> % </p>
        <br/>

        <h3> Voici le classement des trois meilleurs joueurs : </h3>
        <?php
        foreach($classement as $resultat)
        {
          ?>
          <p> Joueur : <?php echo $resultat["pseudo"]; ?> </p>
          <p> Nombre de partie joué : <?php echo $resultat["nombrePartie"]; ?> </p>
          <p> Nombre de partie gagnée : <?php echo $resultat["partieGagnee"]; ?> </p>
          <p> Nombre de partie perdu : <?php echo $resultat["partiePerdu"]; ?> </p>
          <p> Pourcentage de partie gagnée : <?php echo $resultat["pourcentageGagnee"]; ?> % </p>
          <p> Pourcentage de partie perdu : <?php echo $resultat["pourcentagePerdu"]; ?> % </p>
          <br/>
          <p>*********************************************************</p>
          <br/>
          <?php
        }
        ?>

        <br/>
        <a href="index.php?Quitter">Quitter le jeu</a>
        <br/>
        <br/>
        <a href="index.php?Rejouer">Rejouer</a>

      </body>

    </html>

    <?php
  }
}
?>
