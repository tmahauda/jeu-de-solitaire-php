<?php

/**
* Cette classe permet d'afficher la partie avec principalement le plateau du jeu
* @author MAHAUDA Théo, DEGREZ Clara
* @category Partie
* @package Vue
* @version v.1.0 (03/12/2017)
*/
class VuePartie
{

  /**
  * Méthode qui affiche la partie à travers une page HTML
  * @param <string> $message représente un texte personnalisé entre chaque requête émise
  * @param <string> $pseudo représente le pseudo du joueur qui est entrain de jouer
  * @param <boolean[8][8]> $plateau représente le plateau
  */
  function afficherPartie($message, $pseudo, $plateau)
  {
    header('Content-Type: text/html; charset=utf-8');
    ?>
    <!doctype html>

    <html>
      <head>
        <link href="https://fonts.googleapis.com/css?family=Chewy|Dosis|Love+Ya+Like+A+Sister|Michroma|Monoton|Nosifer|Poiret+One|Press+Start+2P|Raleway|Special+Elite" rel="stylesheet">
        <meta charset="utf-8">
        <title> Partie </title>

        <style media="screen">

        h1
        {
          font-size: 50px;
          font-family: 'Press Start 2P', cursive;
          text-align: center;
          color:#014ACA;
        }

        h3
        {
          font-family: 'Dosis', sans-serif;
          color:#014ACA;
          text-align: center;
        }

        table
        {
          font-family: 'Raleway', sans-serif;
          text-align: center;
          margin: auto;
          border: 2;
          width: 600px;
        }

        p
        {
          font-family: 'Poiret One', cursive;
        }

        td
        {
            background: grey;
        }

        #partie
        {
          margin-top: 7%;
          align-items: center;
          display:flex;
          justify-content: space-around;
        }

        #coordonnee
        {
          margin-left: 10px;
        }

        </style>

      </head>

      <body>

        <h1>Le Solitaire</h1>
        <h3> <?php echo $message; ?> </h3>

        <div id="partie">

          <div>

            <table>

              <?php
              for($j=0 ; $j<8 ; $j++)
              {
              ?>

                <tr>

                  <?php
                  for($i=0 ; $i<8 ; $i++)
                  {
                    if(($j==1 && $i==1) || ($j==1 && $i==2) || ($j==1 && $i==6) || ($j==1 && $i==7)
                    || ($j==2 && $i==1) || ($j==2 && $i==2) || ($j==2 && $i==6) || ($j==2 && $i==7)
                    || ($j==6 && $i==1) || ($j==6 && $i==2) || ($j==6 && $i==6) || ($j==6 && $i==7)
                    || ($j==7 && $i==1) || ($j==7 && $i==2) || ($j==7 && $i==6) || ($j==7 && $i==7))
                    {
                      ?>
                      <td style="background:black;width:75px;height:75px;"></td>
                      <?php
                    }
                    else if($j==0)
                    {
                     echo "<td style=\"background:#014ACA;color:white;width:10px;height:10px;\">".$i."</td>";
                    }
                    else if($i==0)
                    {
                     echo "<td style=\"background:#014ACA;color:white;width:10px;height:10px;\">".$j."</td>";
                    }
                    //Si il n'y a pas de bille, on affiche une case grise
                    else if(!$plateau[$j][$i])
                    {
                      ?>
                      <td style="color:grey;width:75px;height:75px;"></td>
                      <?php
                    }
                    // Sinon on affiche une bille
                    else
                    {
                      ?>
                      <td><img src="vue/Image/bille.png" style="width:75px;height:75px;"></td>
                      <?php
                    }
                  }
                  ?>

                </tr>

              <?php
              }
              ?>

            </table>

          </div>

          </br>
          </br>

          <div id="coordonnee">

              <?php
              if(isset($_SESSION["plateau"]))
              {
              ?>
                <form action="index.php" method="post">
                  <h3> Veuillez remplir les coordonnées <?php echo $pseudo ?> </h3>
                <h3>Coordonnées de la bille à déplacer</h3>
                  <p>Numéro de ligne</p>  <input type="text" name="ligneDep"/>
                  <p>Numéro de colonne</p>  <input type="text" name="colonneDep"/>
                <h3>Coordonnées de la case à remplir</h3>
                  <p>Numéro de ligne</p>  <input type="text" name="ligneArr"/>
                  <p>Numéro de colonne</p>  <input type="text" name="colonneArr"/>
                  <input type="submit" name="soumettre" value="OK"/>
                  </form>
                  <?php
                  if(isset($_SESSION["plateauTMP"]))
                  {
                    ?>
                    <form action="index.php" method="post">
                    <br/>
                    <br/>
                    <input type="submit" name="Annuler" value="Annuler le coup d'avant"/>
                    </form>
                    <?php
                  }
              }
              else
              {
              ?>
                <form action="index.php" method="post">
                <h3>Quelle bille voulez-vous enlever en premier <?php echo $pseudo ?> ? </h3>
                  <p>Numéro de ligne</p>  <input type="text" name="ligneSup"/>
                  <p>Numéro de colonne</p>  <input type="text" name="colonneSup"/>
                  <input type="submit" name="soumettre" value="OK"/>
                </form>
              <?php
              }
              ?>
              <br/>
              <form method="post" action="index.php">
              <input type="submit" name="Quitter" value="Quitter la partie"/>
              <br/>
              <br/>
              <input type="submit" name="Reinitialiser" value="Réinitialiser la partie"/>
              </form>
          </div>

        </div>

        <br/>
        <br/>

      </body>

    </html>

    <?php
  }

}

?>
