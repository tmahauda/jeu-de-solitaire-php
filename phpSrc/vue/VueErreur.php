<?php

/**
* Cette classe permet d'afficher divers erreurs issues de l'acceuil ou partie
* @author MAHAUDA Théo, DEGREZ Clara
* @category Erreur
* @package Vue
* @version v.1.0 (03/12/2017)
*/
class VueErreur
{

  /**
  * Méthode qui affiche une erreur issue de l'accueil à travers une page HTML
  * @param <string> $message qui affiche le message d'erreur
  */
  function afficherErreurAccueil($message)
  {
    header('Content-Type: text/html; charset=utf-8');
    ?>
    <!doctype html>

    <html>

      <head>
        <meta charset="utf-8">
        <title> Erreur Accueil </title>
      </head>

      <body>
        <h1> Erreur Accueil </h1>
      <form method="post" action="index.php">
        <p> <?php echo $message; ?> </p>
        <input type="submit" name="Accueil" value="Revenir"/>
      </form>
    </body>

      </html>
    <?php
  }

  /**
  * Méthode qui affiche une erreur issue de la partie à travers une page HTML
  * @param <string> $message qui affiche le message d'erreur
  */
  function afficherErreurPartie($message)
  {
    header('Content-Type: text/html; charset=utf-8');
    ?>
    <!doctype html>

    <html>

      <head>
        <meta charset="utf-8">
        <title> Erreur Partie </title>
      </head>

      <body>
        <h1> Erreur Partie </h1>
      <form method="post" action="index.php">
        <p> <?php echo $message; ?> </p>
        <input type="submit" name="Partie" value="Revenir"/>
      </form>
    </body>

      </html>
    <?php
  }

}

?>
