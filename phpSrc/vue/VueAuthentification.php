<?php

/**
* Cette classe permet d'afficher l'authentification dans accueil
* @author MAHAUDA Théo, DEGREZ Clara
* @category Authentification
* @package Vue
* @version v.1.0 (03/12/2017)
*/
class VueAuthentification
{

  /**
  * Méthode qui affiche l'accueil à travers une page HTML
  * @param <string> $message qui affiche un texte personnalisé entre chaque requête émise
  */
  public function afficherAuthentification($message)
  {
    header('Content-Type: text/html; charset=utf-8');
    ?>
    <!doctype html>

    <html>

      <head>
        <meta charset="utf-8" />
        <title> Accueil </title>

        <style media="screen">

          img
          {
            display: block;
            margin: auto;
            width: 400px;
            height: 300px;
          }

          #information
          {
            display: flex;
            flex-direction: row;
            flex-wrap: nowrap;
            justify-content: space-around;
          }

        </style>

      </head>

      <body>

        <h1 align="center"> <?php echo $message ?> </h1>
        <img src="vue/Image/Solitaire.jpg"/>

        </br>
        </br>
        </br>
        </br>

        <div id="information">

          <div>
            <h4> Connexion </h4>
            <br/>
            <form method="post" action="index.php">
            Pseudo : <input type="text" name="Cpseudo"/>
            <br/>
            <br/>
            Mot de passe : <input type="password" name="Cmdp"/>
            <br/>
            <br/>
            <input type="submit" name="soumettre" value="Se connecter"/>
            </form>
            <br/>
            <br/>
          </div>

        <div>
          <h4> Inscription </h4>
          <br/>
          <form method="post" action="index.php">
          Pseudo : <input type="text" name="Ipseudo"/>
          <br/>
          <br/>
          Mot de passe : <input type="password" name="Imdp"/>
          <br/>
          <br/>
          Confirmer mot de passe : <input type="password" name="Icmdp"/>
          <br/>
          <br/>
          <input type="submit" name="soumettre" value="S'inscrire"/>
          </form>
          <br/>
          <br/>
        </div>

      </div>

      </body>

    </html>
    <?php
  }

}

?>
