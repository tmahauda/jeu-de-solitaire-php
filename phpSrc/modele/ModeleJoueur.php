<?php
require_once "BD.php";

/**
* Cette classe permet de lancer une exception en rapport avec le joueur
* @author MAHAUDA Théo, DEGREZ Clara
* @category Joueur
* @package Modele
* @version v.1.0 (03/12/2017)
*/
class ExceptionJoueur extends Exception
{
  /**
  * Constructeur qui crée l'exception relative au joueur
  * @param <string> $message représente le message d'erreur
  */
  public function __construct($message)
  {
    $this->message = $message;
  }
}

/**
* Cette classe permet de creer un nouveau joueur et de l'ajouter dans la BD
* @author MAHAUDA Théo, DEGREZ Clara
* @category Joueur
* @package Modele
* @version v.1.0 (03/12/2017)
*/
class ModeleNewJoueur
{
  /**
  * Constructeur qui inscrit le joueur dans la BD pour permettre de se connecter ultérieurement
  * @param <string> $pseudo représente le pseudo du nouveau joueur
  * @param <string> $mdp représente le mot de passe du nouveau joueur
  */
  public function __construct($pseudo,$mdp)
  {
    try
    {
      $bd = new BD();
      if(!$bd->chercherJoueur($pseudo))
      {
        $bd->deconnecter();
        $bd->inscrireJoueur($pseudo,$mdp);
      }
      else
      {
        $bd->deconnecter();
        throw new ExceptionJoueur("Ce joueur est déjà inscrit");
      }
    }
    catch(ConnexionException $e)
    {
      $bd->deconnecter();
      print($e->getMessage());
    }
    catch(TableAccesException $e)
    {
      $bd->deconnecter();
      print($e->getMessage());
    }
  }
}

/**
* Cette classe permet de creer un joueur déjà existant dans la BD
* @author MAHAUDA Théo, DEGREZ Clara
* @category Joueur
* @package Modele
* @version v.1.0 (03/12/2017)
*/
class ModeleJoueur
{

  /**
  * @var <string> $pseudo représente le pseudo du joueur
  */
  private $pseudo;

  /**
  * Constructeur qui recherche si le joueur existe dans la BD pour permettre de jouer à une partie
  * @param <string> $pseudo représente le pseudo du joueur
  * @param <string> $mdp représente le mot de passe du joueur
  * @throws ExceptionJoueur si le joueur n'a pas été trouvé à cause du pseudo et/ou mot de passe
  */
  public function __construct($pseudo,$mdp)
  {
    try
    {
      $bd = new BD();
      if($bd->trouverJoueur($pseudo,$mdp))
      {
        $bd->deconnecter();
        $this->pseudo = $pseudo;
      }
      else
      {
        $bd->deconnecter();
        throw new ExceptionJoueur("Le pseudo ou le mot est incorrect");
      }
    }
    catch(ConnexionException $e)
    {
      $bd->deconnecter();
      print($e->getMessage());
    }
    catch(TableAccesException $e)
    {
      $bd->deconnecter();
      print($e->getMessage());
    }
  }

  /**
  * Sélecteur qui permet de récupérer le pseudo du joueur
  * @return <string> $this->pseudo
  */
  public function getPseudo()
  {
    return $this->pseudo;
  }

  /**
  * Méthode qui permet d'enregistrer la performance du joueur par rapport à la partie
  * @param <boolean> $gagnee représente une victoire ou une défaite
  */
  public function enregistrerPerformance($gagnee)
  {
    try
    {
      $bd = new BD();
      $bd->enregistrerPerformanceJoueur($this->pseudo,$gagnee);
      $bd->deconnecter();
    }
    catch(ConnexionException $e)
    {
      $bd->deconnecter();
      print($e->getMessage());
    }
    catch(TableAccesException $e)
    {
      $bd->deconnecter();
      print($e->getMessage());
    }
  }

  /**
  * Méthode qui permet de récupérer les résultats du joueur par l'intermédiaire d'un tableau associatif
  * @return <tableauAssociatif> $resultats
  */
  public function getResultats()
  {
    try
    {
      $bd = new BD();
      $resultats = $bd->getResultatsJoueur($this->pseudo);
      $bd->deconnecter();
      return $resultats;
    }
    catch(ConnexionException $e)
    {
      $bd->deconnecter();
      print($e->getMessage());
    }
    catch(TableAccesException $e)
    {
      $bd->deconnecter();
      print($e->getMessage());
    }
  }

  /**
  * Méthode qui permet de récupérer le classement des 3 meilleurs joueurs avec leurs résultats
  * @return <tableauAssociatif> $classement des 3 meilleurs joueurs
  */
  public function getClassement()
  {
    try
    {
      $bd = new BD();
      $classement = $bd->getClassementJoueurs();
      $bd->deconnecter();
      return $classement;
    }
    catch(ConnexionException $e)
    {
      $bd->deconnecter();
      print($e->getMessage());
    }
    catch(TableAccesException $e)
    {
      $bd->deconnecter();
      print($e->getMessage());
    }
  }
}
?>
