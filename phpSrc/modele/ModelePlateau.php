<?php

/**
* Cette classe permet de lancer une exception général relative au plateau
* @author MAHAUDA Théo, DEGREZ Clara
* @category Plateau
* @package Modele
* @version v.1.0 (03/12/2017)
*/
class ExceptionPlateau extends Exception
{
  /**
  * Constructeur qui crée l'exception relative au plateau
  * @param <string> $message représente le message d'erreur
  */
  public function __construct($message)
  {
    $this->message = $message;
  }
}

/**
* Cette classe héritée de l'exception général au plateau permet de lancer une exception relative aux billes du plateau
* @author MAHAUDA Théo, DEGREZ Clara
* @category Plateau
* @package Modele
*/
class ExceptionBille extends ExceptionPlateau{}

/**
* Cette classe héritée de l'exception général au plateau permet de lancer une exception fin de partie
* @author MAHAUDA Théo, DEGREZ Clara
* @category Plateau
* @package Modele
*/
class ExceptionTermine extends ExceptionPlateau{}

/**
* Cette classe permet de créer un plateau
* @author MAHAUDA Théo, DEGREZ Clara
* @category Plateau
* @package Modele
*/
class ModelePlateau
{
  /**
  * @var <boolean[8][8]> $billes représente une matrice de taille 8x8 remplie de billes ou non, qui sont représentés par des boolean
  */
  private $billes;

  /**
  * @var <boolean> $gagnee représente une victoire ou une défaite
  */
  private $gagnee;

  /**
  * Constructeur qui crée un nouveau plateau de taille 8x8 représenté par la matrice booléenne $billes
  */
  public function __construct()
  {
    $this->billes = array();
    for($j=0 ; $j<8 ; $j++)
    {
      $this->billes[$j] = array();
      for($i=0 ; $i<8 ; $i++)
      {
        if(($j==1 && $i==1) || ($j==1 && $i==2) || ($j==1 && $i==6) || ($j==1 && $i==7)
        || ($j==2 && $i==1) || ($j==2 && $i==2) || ($j==2 && $i==6) || ($j==2 && $i==7)
        || ($j==6 && $i==1) || ($j==6 && $i==2) || ($j==6 && $i==6) || ($j==6 && $i==7)
        || ($j==7 && $i==1) || ($j==7 && $i==2) || ($j==7 && $i==6) || ($j==7 && $i==7)
        || $j==0 || $i==0)
        {
          // Aucune présence de bille
          $this->billes[$j][$i] = false;
        }
        // Sinon présence d'une bille
        else $this->billes[$j][$i] = true;
      }
    }
  }

  /**
  * Sélecteur qui permet de récupérer le plateau représenté par la matrice booléenne $billes
  * @return <boolean[8][8]> $this->billes
  */
  public function getPlateau()
  {
    return $this->billes;
  }

  /**
  * Modificateur qui permet de modifier l'état du plateau représenté par la matrice booléenne $billes
  * @param <boolean[8][8]> $billes représente le nouveau plateau
  */
  public function setPlateau($billes)
  {
    $this->billes = $billes;
  }

  /**
  * Sélecteur qui permet de récupérer la présence d'une bille sur le plateau par ses coordonnées x et y
  * @param <integer> $ligne représente la coordonnée sur x du plateau
  * @param <integer> $colonne représente la coordonnée sur y du plateau
  * @return <boolean> $this->billes[$ligne][$colonne] qui représente la présence ou non de la bille
  */
  public function getBille($ligne, $colonne)
  {
    return $this->billes[$ligne][$colonne];
  }

  /**
  * Modificateur qui permet de changer la présence d'une bille sur le plateau par ses coordonnées x et y
  * @param <integer> $ligne représente la coordonnée sur x du plateau
  * @param <integer> $colonne représente la coordonnée sur y du plateau
  * @param <boolean> $etat représente l'état de la bille, si présente ou non
  */
  public function setBille($ligne, $colonne, $etat)
  {
    $this->billes[$ligne][$colonne] = $etat;
  }

  /**
  * Sélecteur qui permet de récupérer une victoire ou une défaite par rapport au plateau
  * @return <boolean> $this->gagnee
  */
  public function getGagnee()
  {
    return $this->gagnee;
  }

  /**
  * Méthode qui permet d'enlever une bille existante par ses coordonnées x et y du plateau en suivant des régles bien définies :
  * - Il faut que les coordonnées soit remplies
  * - Il faut que les coordonnées respectent la plage de 1 à 7
  * - Il faut que la bille sélectionnée existe (bien évidemment, on ne peut pas supprimer une bille qui n'existe pas)
  * @param <integer> $ligne représente la coordonnée sur x de la bille sur le plateau
  * @param <integer> $colonne représente la coordonnée sur y de la bille sur le plateau
  * @throws ExceptionBille si la bille n'a pu être supprimée
  */
  public function enleverBille($ligne, $colonne)
  {
    // Si les variables ne sont pas vide
    if(!empty($ligne) && !empty($colonne))
    {
      // Si les coordonnées sont dans la plage de 1 à 7
      if($ligne > 0 && $ligne < 8 && $colonne > 0 && $colonne < 8)
      {
        // Si la bille existe
        if($this->billes[$ligne][$colonne])
        {
          //On supprime la bille
          $this->billes[$ligne][$colonne] = false;
        }
        else
        {
          throw new ExceptionBille("Il n'y a pas de bille !");
        }
      }
      else
      {
        throw new ExceptionBille("Les coordonnées fournises ne sont pas dans la plage valide");
      }
    }
    else
    {
      throw new ExceptionBille("Les champs des coordonnées n'ont pas tous été complétés");
    }
  }

  /**
  * Méthode qui permet de déplacer une bille existante par ses coordonnées x et y à une autre case non occupée par une autre bille de coordonnées x et y du plateau en suivant des régles bien définies :
  * - Il faut que toutes les coordonnées soit remplies
  * - Il faut que toutes les coordonnées respectent la plage de 1 à 7
  * - Il faut que la bille que l'on veut déplacer soit présente
  * - Il faut que la case d'arrivé soit vide (non occupé par une autre bille)
  * - Il faut que le déplacement de la bille ne fasse pas en diagonale
  * - Il faut que la bille "écrase" une autre bille durant ce déplacement
  * @param <integer> $ligneDep représente la coordonnée sur x de bille d'origine du plateau
  * @param <integer> $colonneDep représente la coordonnée sur y bille d'origine du plateau
  * @param <integer> $ligneArr représente la coordonnée sur x de la case objective du plateau
  * @param <integer> $colonneArr représente la coordonnée sur y de la case objective du plateau
  * @throws ExceptionBille si la bille n'a pu être déplacée
  */
  public function deplacerBille($ligneDep,$colonneDep,$ligneArr,$colonneArr)
  {
    //Si les champs ne sont pas vides
    if(!empty($ligneDep) && !empty($colonneDep) && !empty($ligneArr) && !empty($colonneArr))
    {
      //Si les coordonnées sont de 1 à 7
      if($ligneDep > 0 && $ligneDep < 8 && $ligneArr > 0 && $ligneArr < 8
      && $colonneDep > 0 && $colonneDep < 8 && $colonneArr > 0 && $colonneArr < 8)
      {
        //S'il existe une bille
        if($this->billes[$ligneDep][$colonneDep])
        {
          //S'il n'y a pas de bille à l'arrivé
          if(!$this->billes[$ligneArr][$colonneArr])
          {
            //Si le déplacement n'est pas en diagonale
            if(($ligneDep - 2 == $ligneArr && $colonneDep == $colonneArr) //Si déplacement horizontal vers le bas
            || ($ligneDep + 2 == $ligneArr && $colonneDep == $colonneArr) //Si déplacement horizontal vers le haut
            || ($ligneDep == $ligneArr && $colonneDep - 2 == $colonneArr) //Si déplacement vertical vers le bas
            || ($ligneDep == $ligneArr && $colonneDep + 2 == $colonneArr)) //Si déplacement vertical vers le haut
            {
              //Si durant le déplacement on croise une bille
              if($this->billes[($ligneDep + $ligneArr) / 2][($colonneDep + $colonneArr) / 2])
              {
                //Alors on peut effectuer le déplacement

                //On supprime la bille A de sa case d'origine
                $this->billes[$ligneDep][$colonneDep] = false;
                //On supprime la bille B issue du déplacement de la bille A
                $this->billes[($ligneDep + $ligneArr) / 2][($colonneDep + $colonneArr) / 2] = false;
                //On actualise la bille A sur sa nouvelle case
                $this->billes[$ligneArr][$colonneArr] = true;
              }

              //Sinon on lève une erreur
              else
              {
                throw new ExceptionBille("Vous avez essayé(e) de déplacer une bille sans ôter une autre bille");
              }
            }
            else
            {
              throw new ExceptionBille("Vous n'avez pas déplacé(e) la bille de deux cases uniquement en vertical ou en horizontal");
            }
          }
          else
          {
            throw new ExceptionBille("Il y a déjà une bille !");
          }
        }
        else
        {
          throw new ExceptionBille("Il n'y a pas de bille !");
        }
      }
      else
      {
        throw new ExceptionBille("Les coordonnées fournies ne sont pas dans la plage valide");
      }
    }
    else
    {
      throw new ExceptionBille("Les champs des coordonnées n'ont pas tous été complétés");
    }
  }

  /**
  * Méthode qui permet de savoir s'il existe une unique bille sur le plateau. C'est à dire savoir si on a gagné
  * @throws ExceptionTermine si on a remporté la partie
  */
  public function existeUneUniqueBille()
  {
    $nombreBilles = 0;
    for($j=0 ; $j<8 ; $j++)
    {
      for($i=0 ; $i<8 ; $i++)
      {
        if($this->billes[$j][$i])
        {
          $nombreBilles = $nombreBilles + 1;
        }
      }
    }
    // S'il reste une et une seule bille, alors on a gagnée
    if($nombreBilles == 1)
    {
      $this->gagnee = true;
      throw new ExceptionTermine("Bravo vous avez gagné !");
    }
  }

  /**
  * Méthode qui permet de savoir s'il reste au moins un coup possible de déplacement sur le plateau. C'est à dire savoir si on a perdu
  * @throws ExceptionTermine si on a perdu la partie
  */
  public function existeCoupPossible()
  {
    $coupPossible = false;
    for($j=0 ; $j<8 ; $j++)
    {
      for($i=0 ; $i<8 ; $i++)
      {
        // S'il existe une bille courante
        if($this->billes[$j][$i])
        {
          if( ($j+2<8 && $this->billes[$j+1][$i] && !$this->billes[$j+2][$i])   // Et s'il existe une bille à droite avec un emplacement vide de la bille courante
          ||  ($j-2>0 && $this->billes[$j-1][$i] && !$this->billes[$j-2][$i])   // Ou s'il existe une bille à gauche avec un emplacement vide de la bille courante
          ||  ($i+2<8 && $this->billes[$j][$i+1] && !$this->billes[$j][$i+2])   // Ou s'il existe une bille en haut avec un emplacement vide de la bille courante
          ||  ($i-2>0 && $this->billes[$j][$i-1] && !$this->billes[$j][$i-2]) ) // Ou s'il existe une bille en bas avec un emplacement vide de la bille courante
          {
            // Il reste au moins un coup possible
            $coupPossible = true;
          }
        }
      }
    }
    // S'il ne reste plus de coup possible, alors on a perdu
    if(!$coupPossible)
    {
      $this->gagnee = false;
      throw new ExceptionTermine("Mince vous avez perdu !");
    }
  }

}

 ?>
