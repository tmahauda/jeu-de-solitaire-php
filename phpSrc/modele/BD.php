<?php

/**
* Cette classe permet de définir une exception général relative à la BD
* @author MAHAUDA Théo, DEGREZ Clara
* @category BD
* @package Modele
* @version v.1.0 (03/12/2017)
*/
class ExceptionBD extends Exception
{
  /**
  * Constructeur qui crée l'exception relative à la BD (soit un problème relative à une connexion ou d'accès à une table)
  * @param <string> $message représente le message d'erreur
  */
  public function __construct($message)
  {
    $this->message=$message;
  }
}

/**
* Cette classe hérité de l'exception général à la BD permet de définir une exception relative à un probleme de connexion
* @author MAHAUDA Théo, DEGREZ Clara
* @category Joueur
* @package Modele
* @version v.1.0 (03/12/2017)
*/
class ConnexionException extends ExceptionBD{}

/**
* Cette classe hérité de l'exception général à la BD permet de définir une exception relative à un probleme d'accès à une table
* @author MAHAUDA Théo, DEGREZ Clara
* @category Joueur
* @package Modele
*/
class TableAccesException extends ExceptionBD{}

/**
* Cette classe permet de se connecter à la base de données (BD) relative au joueur principalement
* @author MAHAUDA Théo, DEGREZ Clara
* @category Joueur
* @package Modele
* @version v.1.0 (03/12/2017)
*/
class BD
{
  /**
  * @var <PDO> $connexion représente la chaîne de connexion avec la BD
  */
  private $connexion;

  /**
  * Constructeur qui crée la chaine de connexion avec la BD
  * @throws ConnexionException s'il y a eu un problème de connexion
  */
  public function __construct()
  {
     try
     {
        $chaine="mysql:host=infoweb;dbname=E164359B";
        $this->connexion = new PDO($chaine,"E164359B","E164359B");
        $this->connexion->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
      }
      catch(PDOException $e)
      {
        throw ConnexionException("Problème de connexion à la base : ".$e->getMessage());
      }
  }

  /**
  * Méthode qui permet de se déconnecter en brisant la chaîne de connexion
  */
  public function deconnecter()
  {
     $this->connexion=null;
  }

  /**
  * Méthode qui permet de chercher un joueur par son pseudo et son mot de passe
  * @param <string> $pseudo représente le pseudo du joueur
  * @param <string> $mdp représente le mot de passe du joueur
  * @return <boolean> $trouver représente le joueur trouvé ou non
  * @throws TableAccesException s'il y a eu un problème d'accès à la table joueurs
  */
  public function trouverJoueur($pseudo,$mdp)
  {
    try
    {
      $stmt = $this->connexion->prepare("select motDePasse from joueurs where pseudo=?");
      $stmt->bindParam(1, $pseudo);
      $stmt->execute();
      $resultat= $stmt->fetch();
      if(isset($resultat['motDePasse']))
      {
        return crypt($mdp, $resultat['motDePasse']) == $resultat['motDePasse'];
      }
      else
      {
        return false;
      }
    }
    catch(PDOException $e)
    {
      throw TableAccesException("Problème d'accées à la table joueurs : ".$e->getMessage());
    }
  }

  /**
  * Méthode qui permet de chercher un joueur par son pseudo uniquement (le mécanisme de surcharge ne fonctionne pas car on aurait pu faire public function trouverJoueur($pseudo))
  * @param <string> $pseudo représente le pseudo du joueur
  * @return <boolean> $trouver représente le joueur trouvé ou non
  * @throws TableAccesException s'il y a eu un problème d'accès à la table joueurs
  */
  public function chercherJoueur($pseudo)
  {
    try
    {
      $stmt = $this->connexion->prepare("select pseudo from joueurs where pseudo=?");
      $stmt->bindParam(1, $pseudo);
      $stmt->execute();
      $resultat= $stmt->fetch();
      if(isset($resultat['pseudo']))
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    catch(PDOException $e)
    {
      throw TableAccesException("Problème d'accées à la table joueurs : ".$e->getMessage());
    }
  }

  /**
  * Méthode qui permet d'inscrire un nouveau joueur
  * @param <string> $pseudo représente le pseudo du nouveau joueur
  * @param <string> $mdp représente le mot de passe du nouveau joueur
  * @throws TableAccesException s'il y a eu un problème d'accès à la table joueurs
  */
  public function inscrireJoueur($pseudo,$mdp)
  {
    try
    {
        $stmt = $this->connexion->prepare("insert into joueurs(pseudo,motDePasse) value(?,?)");
        $stmt->bindParam(1, $pseudo);
        $stmt->bindParam(2, crypt($mdp));
        $stmt->execute();
    }
    catch(PDOException $e)
    {
      throw TableAccesException("problème d'accées à la table joueurs : ".$e->getMessage());
    }
  }

  /**
  * Méthode qui permet d'enregistrer les performances du joueur lors de la fin de partie
  * @param <string> $pseudo représente le pseudo du joueur
  * @param <boolean> $gagnee représente une victoire ou défaite du joueur
  * @throws TableAccesException s'il y a eu un problème d'accès à la table parties
  */
  public function enregistrerPerformanceJoueur($pseudo, $gagnee)
  {
    try
    {
        $stmt = $this->connexion->prepare("insert into parties(pseudo,partieGagnee) value(?,?)");
        $stmt->bindParam(1, $pseudo);
        $stmt->bindParam(2, $gagnee);
        $stmt->execute();
    }
    catch(PDOException $e)
    {
      throw TableAccesException("Problème d'accées à la table parties : ".$e->getMessage());
    }
  }

  /**
  * Méthode qui permet de récupérer les résultats d'un joueur sur l'ensemble des parties jouées
  * @param <string> $pseudo représente le pseudo du joueur
  * @return <tableauAssociatif> $stmt->fetch() représente les performances du joueur
  * @throws TableAccesException s'il y a eu un problème d'accès à la table parties
  */
  public function getResultatsJoueur($pseudo)
  {
    try
    {
      $requete = "SELECT pseudo, COUNT(*) AS nombrePartie, SUM(partieGagnee) AS partieGagnee, (COUNT(*) - SUM(partieGagnee)) AS partiePerdu, (FLOOR((SUM(partieGagnee) / COUNT(*)) * 100)) AS pourcentageGagnee, (FLOOR(((COUNT(*) - SUM(partieGagnee)) / COUNT(*)) * 100)) AS pourcentagePerdu FROM parties GROUP BY pseudo HAVING pseudo = ?";
      $stmt = $this->connexion->prepare($requete);
      $stmt->bindParam(1, $pseudo);
      $stmt->execute();
      return $stmt->fetch();
    }
    catch(PDOException $e)
    {
      throw TableAccesException("Problème d'accées à la table parties : ".$e->getMessage());
    }
  }

  /**
  * Méthode qui permet de récupérer le classement des 3 meilleurs joueurs (classé du plus grand nombre de partie remporté jusqu'au plus petit) inscrits avec leurs résultats
  * @return <tableauAssociatif> $stmt->fetchAll() représente le résultat de la requête
  * @throws TableAccesException s'il y a eu un problème d'accès à la table parties
  */
  public function getClassementJoueurs()
  {
    try
    {
      $requete = "SELECT pseudo, COUNT(*) AS nombrePartie, SUM(partieGagnee) AS partieGagnee, (COUNT(*) - SUM(partieGagnee)) AS partiePerdu, (FLOOR((SUM(partieGagnee) / COUNT(*)) * 100)) AS pourcentageGagnee, (FLOOR(((COUNT(*) - SUM(partieGagnee)) / COUNT(*)) * 100)) AS pourcentagePerdu FROM parties GROUP BY pseudo ORDER BY 3 DESC LIMIT 3";
      $stmt = $this->connexion->prepare($requete);
      $stmt->execute();
      return $stmt->fetchAll();
    }
    catch(PDOException $e)
    {
      throw TableAccesException("Problème d'accées à la table parties : ".$e->getMessage());
    }
  }
}
?>
