# Jeu de Solitaire

<div align="center">
<img width="500" height="400" src="Solitaire.jpg">
</div>

## Description du projet

Application web réalisée avec PHP en DUT INFO 2 à l'IUT de Nantes dans le cadre du module "Programmation Web coté serveur" durant l'année 2017-2018 avec un groupe de deux personnes. \
L'objectif du jeu est de supprimer des billes du plateau afin qu'à la fin il n'en reste plus qu'une. \
Pour qu'une bille soit ôtée du plateau, on doit sauter par dessus avec une autre bille en direction d'une case vide. \
La prise en diagonale est interdite. L'utilisateur aura le choix de la première bille qu'il va ôter de la grille pour pouvoir commencer à jouer.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par un groupe de deux étudiants de l'IUT de Nantes :
- Théo MAHAUDA : theo.mahauda@etu.univ-nantes.fr ;
- Clara DEGREZ : clara.degrez@etu.univ-nantes.fr.

### Encadrants

Ce projet fut encadré par une enseignante de l'IUT de Nantes :
- Christine JACQUIN : jacquin.christine@univ-nantes.fr.

## Organisation

Ce projet a été agit au sein de l'université de Nantes dans le cadre du module "Programmation Web coté serveur" du DUT INFO 2.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2017 sur la période du module pendant les heures de TP et personnelles à la maison. \
Il a été terminé et rendu le 10/11/2017.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- PHP ;
- Visual Studio Code ;
- MVC.

## Objectifs

La finalité de ce projet est de pouvoir jouer au jeu de Solitaire.
